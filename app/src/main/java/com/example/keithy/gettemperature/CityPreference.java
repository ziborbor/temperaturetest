package com.example.keithy.gettemperature;

/**
 * Created by Keithy on 3/01/2015.
 */
import android.app.Activity;
import android.content.SharedPreferences;

public class CityPreference {

    SharedPreferences prefs;

    public CityPreference(Activity activity){
        prefs = activity.getPreferences(Activity.MODE_PRIVATE);
    }

    // If the user has not chosen a city yet, return
    // Auckland as the default city
    String getCity(){
        return prefs.getString("city", "Auckland, NZ");
    }

    void setCity(String city){
        prefs.edit().putString("city", city).commit();
    }

}